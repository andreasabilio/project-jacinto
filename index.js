
// Deps
var koa    = require('koa');
var path   = require('path');
var Static = require('koa-static');

var server = koa();
var app    = path.resolve(__dirname, './app');
var build  = path.resolve(__dirname, './build');

server.use(Static(app));
server.use(Static(build));

// Launch the server
server.listen(3000);

