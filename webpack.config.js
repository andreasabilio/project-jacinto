
// Get SASS global variables
var sassGlobals = require('clio-sass-globals');

// XXX
// console.log('>>> sassGlobals:', sassGlobals, '\n');

module.exports = {
    entry: './app/app.js',
    output: { path: './build/', filename: 'app.bundle.js' },
    module: {
        loaders: [
            {
                // Babel && React transpiling
                test: /.js$/,
                loader: 'babel-loader',
                exclude: /node_modules/,
                query: { presets: ['es2015', 'react']}
            },
            {
                // SASS compiling && loading
                test: /\.scss$/,
                loaders: ['style', 'css', 'sass']
            },
            {
                // CSS loading
                test: /\.css$/,
                loaders: ['style', 'css?sourceMap']
            },
            {
                // Font loading (via css)
                test: /\.(eot|svg|ttf|woff|woff2)$/,
                loader: 'file?name=public/fonts/[name].[ext]'
            }
        ]
    },
    sassLoader: {
        data: sassGlobals
    }
};